var apiKey = 'ABQIAAAA1UYRHOU-JUNOB141p2_n0BT8PLg0R80HGdbhzNWO7w8UyDJxYBRoRnXoBfeX0t76_MlfYQI1R0cO0A';

var map;
var stores = [];
var markerLocs = [];
var markers = [];
var infoWindowContent = [];
var ib;
var bounds;

function initMap() {
  map = new google.maps.Map(document.getElementById('sc-map-search-map'), {
    center: {lat: 36.346241, lng: -94.1867376},
    zoom: 9,
    gestureHandling: 'cooperative'
  });
  ib = new google.maps.InfoWindow({content: ''});
  bounds = new google.maps.LatLngBounds();

  $('#sc-map-search-map').append('<div class="clear"></div>')
}

/**Marker Functions**/
function createInfoWindow (data) {
  return '<div id="content"> \
        <span style="font-weight:bold; font-size:12px;">Club #: '+ data.StoreNumber +' </span><br /><br /> \
        <div style="font-size:10px; float:left"> \
          '+data.Address+' <br /> \
          '+data.City+', '+data.State+' '+data.PostalCode+' <br /><br /> \
          <a target="_blank" style="color:#2983ea; text-decoration:none;" href="http://maps.google.com/maps?q='+data.Address+', '+data.City+', '+data.State+' '+data.PostalCode+'"> \Get Directions</a> \
        </div> \
      </div>';
}

function createMarker (loc) {
  return new google.maps.Marker({
      position: loc,
      map: map,
      icon: 'https://staging-samsclub.triadretail.net/Global/sc-map-search/images/sc-map-marker.png',
      animation: google.maps.Animation.DROP,
      label: ''
    });
}
function addMarkers () {
  $('.spinner').hide();
  $('#sc-map-results-list, #sc-map-results-list2').empty();
  $('.sc-map-search-results h1').show();

  if(markers.length > 0)
    clearMarkers();

  for( var i = 0, len = stores.length; i < len; i++ ){
    var loc = {
      lat: parseFloat(stores[i].Latitude),
      lng: parseFloat(stores[i].Longitude)
    }
   
    markerLocs[i] = loc;
    markers[i] = createMarker(loc);
    markers[i].setMap(map);
    infoWindowContent[i] = createInfoWindow(stores[i]);
    addInfoBox(markers[i], infoWindowContent[i]);

    $('#sc-map-results-list, #sc-map-results-list2').append(' \
      <a href="" data-lat="'+loc.lat+'" data-lng="'+loc.lng+'" data-index="'+i+'"> \
        '+stores[i].Address+' <br /> \
        '+stores[i].City+', '+stores[i].State+' '+stores[i].PostalCode+' \
      </a>');

    bounds.extend(new google.maps.LatLng(loc.lat, loc.lng));
  }

  map.fitBounds(bounds);
  map.panToBounds(bounds);
  map.setZoom(9); 

  $('#sc-map-results-list a, #sc-map-results-list2 a').on('click', function(e){
    e.preventDefault();
    var recenter = new google.maps.LatLng(parseFloat($(this).attr('data-lat')), parseFloat($(this).attr('data-lng')));
    map.panTo(recenter);
    map.setZoom(16);

    google.maps.event.trigger(markers[parseInt($(this).attr('data-index'))], 'click');
  });

  $('#sc-map-results-list a, #sc-map-results-list2 a').on('mouseenter', function(e){
    markerOver(markers[parseInt($(this).attr('data-index'))])
  });

  $('#sc-map-results-list a, #sc-map-results-list2 a').on('mouseleave', function(e){
    markerOut(markers[parseInt($(this).attr('data-index'))])
  });
}
function markerOver (marker) {
  marker.setAnimation(google.maps.Animation.BOUNCE);
}
function markerOut (marker) {
  marker.setAnimation(null);
}
function addInfoBox (mark, cont) {
  google.maps.event.addListener(mark, 'click', function(){
    ib.setContent(cont);
    ib.open(map, this);
  });
}
function clearMarkers () {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }

  bounds = new google.maps.LatLngBounds(null);
  markerLocs = [];
  markers = [];
  infoWindowContent = [];
}

/**No Locations Found**/
function noLocationsFound () {
  $('.sc-map-search-results h1').show();
  $('#sc-map-results-list').html('<h2>Please try again.</h2>');
  $('.spinner').hide();
}

function finishedTyping (e) {
  var zip = $(this).val();
  if(zip.length == 5 && zip.match(/^[0-9]+$/) != null) {
    Omniture_TrackInternalLink(MAPCONVERGE + '_SearchZip_' + zip + '_US_ENG_01');
    $.getJSON((MAPSERVICEBASE + 'GetStores?Zipcode=' + zip), function(data){
        stores = data;
        $('.num-of-clubs').text(stores.length);
        if (stores.length > 0)
          addMarkers();
        else{
          noLocationsFound();
        }
      });
  } else if (zip.length == 5 && zip.match(/^[0-9]+$/) == null) {
    $('.spinner').show();
    reportError('Invalid Zip!');
  } else {
    $('.spinner').hide();
    $('.error-reporter').hide()
  }
}

function reportError (msg) {
  $('.error-reporter').empty();
  $('.error-reporter').html('<h2>'+msg+'</h2>');
  $('.error-reporter').show();

  setTimeout(function(){$('.error-reporter').fadeOut();}, 3000);
}

$(document).ready(function (){
  var typing = false;
  var timeOut = null;
  $('#zip-box').on('keydown', function(e){
    $('.spinner').show();
    clearTimeout(timeOut);
    typing = true;

    if (e.keyCode == 13)
      return false;
  });

  $('#zip-box').on('keyup', $.debounce(1000, finishedTyping));

});
